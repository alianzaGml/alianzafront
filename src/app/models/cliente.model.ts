export interface Cliente {
  sharedKey: string;
  businessId: string;
  email: string;
  phone: any;
  dataAdded: any;
}