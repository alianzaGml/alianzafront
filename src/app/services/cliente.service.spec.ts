import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ClienteService } from './cliente.service';
import { Cliente } from '../models/cliente.model';

describe('ClienteService', () => {
  let service: ClienteService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ClienteService]
    });
    service = TestBed.inject(ClienteService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('listarClientes() should return data', () => {
    const dummyClientes: Cliente[] = [
      { sharedKey: '001', businessId: 'BIZ100', email: 'test@example.com', phone: '1234567890', dataAdded: '2021-01-01' }
    ];

    service.listarClientes().subscribe(clientes => {
      expect(clientes.length).toBe(1);
      expect(clientes).toEqual(dummyClientes);
    });

    const req = httpTestingController.expectOne(request => request.url.includes('clientes') && request.method === 'GET');
    expect(req.request.method).toBe('GET');
    req.flush(dummyClientes);
  });

  it('guardarCliente() should post and return the client', () => {
    const nuevoCliente: Cliente = { sharedKey: '002', businessId: 'BIZ200', email: 'test2@example.com', phone: '0987654321', dataAdded: '2021-02-01' };

    service.guardarCliente(nuevoCliente).subscribe(cliente => {
      expect(cliente).toEqual(nuevoCliente);
    });

    const req = httpTestingController.expectOne(request => request.url.includes('clientes') && request.method === 'POST');
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(nuevoCliente);
    req.flush(nuevoCliente);
  });

  it('buscarClientePorSharedKey() should return an array of clients', () => {
    const resultadoBusqueda: Cliente[] = [
      { sharedKey: '001', businessId: 'BIZ100', email: 'test@example.com', phone: '1234567890', dataAdded: '2021-01-01' }
    ];

    service.buscarClientePorSharedKey('001').subscribe(clientes => {
      expect(clientes.length).toBe(1);
      expect(clientes).toEqual(resultadoBusqueda);
    });

    const req = httpTestingController.expectOne(request => request.url.includes('clientes/001') && request.method === 'GET');
    expect(req.request.method).toBe('GET');
    req.flush(resultadoBusqueda);
  });

  it('exportarClientesCSV() should download data as a blob', () => {
    const dummyBlob = new Blob(['id,sharedKey,businessId,email,phone,dataAdded\n001,BIZ100,BIZ100,test@example.com,1234567890,2021-01-01'], { type: 'text/csv' });
  
    service.exportarClientesCSV().subscribe(response => {
      expect(response.size).toBe(dummyBlob.size);
      expect(response.type).toBe(dummyBlob.type);
    });
  
    const req = httpTestingController.expectOne(`${service.baseURL}/exportar`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('blob');
    req.flush(dummyBlob);
  });
});
