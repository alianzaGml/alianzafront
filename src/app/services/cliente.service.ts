import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente.model';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  public baseURL = 'http://localhost:8081/api/clientes';

  constructor(private http : HttpClient) { }

  listarClientes(): Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.baseURL}`);
  }

  guardarCliente(cliente : Cliente): Observable<Object>{
    return this.http.post(`${this.baseURL}`, cliente);
  }

  buscarClientePorSharedKey(sharedKey : String): Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.baseURL}/${sharedKey}`);
  }

  exportarClientesCSV(): Observable<any> {
    return this.http.get(`${this.baseURL}/exportar`, { responseType: 'blob' });
  }

  buscarClienteAvanzado(searchParams: any): Observable<Cliente[]> {
    let params = new HttpParams();
    for (const key in searchParams) {
      if (searchParams[key]) {
        params = params.set(key, searchParams[key]);
      }
    }
    return this.http.get<Cliente[]>(`${this.baseURL}/buscar`, { params: params });
  }


}
