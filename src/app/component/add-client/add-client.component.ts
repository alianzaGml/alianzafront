import { Component, EventEmitter, Output } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent {

  @Output() nuevoCliente = new EventEmitter<any>();
  @Output() cancelar = new EventEmitter();
  cliente = { sharedKey: '', businessId: '', email: '', phone: '', dataAdded: '' };

  constructor(private service : ClienteService){}

  guardarCliente(){
    if(this.cliente.sharedKey && this.cliente.businessId && this.cliente.email && this.cliente.phone && this.cliente.dataAdded){
      this.service.guardarCliente(this.cliente).subscribe({
        next: (clienteGuardado) => {
          console.log('Cliente guardado:', clienteGuardado);
          this.nuevoCliente.emit(this.cliente);
          this.cliente = { sharedKey: '', businessId: '', email: '', phone: '', dataAdded: '' };
        },
        error: (error) => {
          console.error('Error al guardar el cliente:', error);
        }
      });
    }
  }

  cancelarCliente(){
    this.cancelar.emit();
  }
}
