import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente.model';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.scss']
})
export class ClienteListComponent implements OnInit{

  clientes : Cliente[] = [];
  filtro = '';
  mostrandoFormulario = false;
  mostrandoAdvanced = false;

  constructor(private service : ClienteService){}

  ngOnInit(): void {
    this.service.listarClientes().subscribe(data => {
      this.clientes = data;
    });
  }

  mostrarFormularioAgregar() {
    this.mostrandoFormulario = true;
  }

  mostrarFormularioAdvanced() {
    this.mostrandoAdvanced = true;
  }

  agregarCliente(cliente: Cliente) {
    this.clientes.push(cliente);
    this.mostrandoFormulario = false;
  }

  buscarClientes(){
    if(!this.filtro){
      this.service.listarClientes().subscribe(data => {
        this.clientes = data;
      });
    }else {
      this.service.buscarClientePorSharedKey(this.filtro).subscribe(data => {
        this.clientes = Array.isArray(data) ? data : [data];
      }, error => {
        console.log('No se encontraron clientes', error);
        this.clientes = [];
      });
    }
  }

  exportarClientesCSV() {
    this.service.exportarClientesCSV().subscribe(response => {
      const blob = new Blob([response], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);
      const anchor = document.createElement('a');
      anchor.download = 'clientes.csv';
      anchor.href = url;
      anchor.click();
      window.URL.revokeObjectURL(url);
    }, error => console.log(error));
  }

}
