import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClienteListComponent } from './component/cliente-list/cliente-list.component';

const routes: Routes = [

  {
    path : '',
    component : ClienteListComponent
  },
  {
    path : 'clientes',
    component : ClienteListComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
